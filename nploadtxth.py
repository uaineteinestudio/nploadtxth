import numpy as np
import os

def skipper(fname,linesToSkip):#skip the starting lines for np load (quicker than standard native library)
        with open(fname) as fin:
            for i, line in enumerate(fin): #for each line
                if i > linesToSkip-1:
                    yield line  #return that row please

def loadtxth(fname, linesTokip):
    return np.loadtxt(skipper(fname, linesTokip))